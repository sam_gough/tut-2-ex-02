﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tut_2_Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = Console.ReadLine();

            switch (colour)
            {
                case "Red":
                    Console.WriteLine("You chose red - Apples can be red");
                    break;

                case "Blue":
                    Console.WriteLine("You chose blue - The Sky is blue");
                    break;

                default:
                    Console.WriteLine("The selection was invalid!");
                    break;                
                           
            }
               
          
        }
    }
}
